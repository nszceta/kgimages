#!/usr/bin/env bash

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export MULTIMEDIA_ROOTDIR="/mnt/Andrew Website/"
export PYTHONPATH="src"
pyenv shell kgimages
pip install --upgrade pip setuptools wheel
pip install --upgrade -r /home/server/kgimages/requirements.txt
python -c "import sys; print(sys.executable)"
cd /home/server/kgimages && python src/kgimages/app.py
