import os
import random
import secrets
from pathlib import Path
from urllib.parse import urlencode

import filetype
import uvicorn
from fastapi import Depends, FastAPI, HTTPException, Request, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger

app = FastAPI()
multimedia_rootdir = Path(os.environ["MULTIMEDIA_ROOTDIR"])
logger.debug(f"{multimedia_rootdir=}")
app.mount("/multimedia", StaticFiles(directory=multimedia_rootdir), name="multimedia")
app.mount("/static", StaticFiles(directory="static"), name="static")
multimedia_paths = []
templates = Jinja2Templates(directory="src/kgimages/templates")
THUMBNAILS_SAMPLES_CNT = 12
security = HTTPBasic()


def authenticate(credentials: HTTPBasicCredentials = Depends(security)):
    current_username_bytes = credentials.username.encode("utf8")
    correct_username_bytes = b"andrew"
    is_correct_username = secrets.compare_digest(
        current_username_bytes, correct_username_bytes
    )
    current_password_bytes = credentials.password.encode("utf8")
    correct_password_bytes = b"andrew"
    is_correct_password = secrets.compare_digest(
        current_password_bytes, correct_password_bytes
    )
    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )


def get_folder_listing(basedir):
    dirs = []
    images = []
    videos = []
    for f in basedir.glob("*"):
        if f.is_file():
            kind = filetype.guess(str(f))
            if kind is not None:
                try:
                    if kind.mime.split("/")[0] == "image":
                        images.append(f.relative_to(multimedia_rootdir).as_posix())
                    elif kind.mime.split("/")[0] == "video":
                        videos.append(f.relative_to(multimedia_rootdir).as_posix())
                except IndexError:
                    pass
        elif f.is_dir():
            dirs.append(f.relative_to(multimedia_rootdir).as_posix())
    return dirs, images, videos


@app.get("/", response_class=RedirectResponse)
async def root(_: HTTPBasicCredentials = Depends(authenticate)):
    return "/items"


@app.get("/items", response_class=HTMLResponse)
async def get_items_by_dir(
    request: Request,
    _: HTTPBasicCredentials = Depends(authenticate),
    multimedia_dir: str = "",
):
    try:
        basedir = multimedia_rootdir / Path(multimedia_rootdir).joinpath(
            multimedia_dir
        ).resolve().relative_to(multimedia_rootdir.resolve())
    except ValueError:
        raise HTTPException(status_code=400)
    dirs, images, videos = get_folder_listing(basedir)
    dirs_final = []
    for dir_name in dirs:
        dir_url = "/items?" + urlencode({"multimedia_dir": str(dir_name)})
        _, images2, _ = get_folder_listing(multimedia_rootdir / dir_name)
        dir_thumbnail_urls = (
            "/multimedia/" + url
            for url in random.sample(images2, min(THUMBNAILS_SAMPLES_CNT, len(images2)))
        )
        dirs_final.append((dir_name, dir_url, dir_thumbnail_urls))
    return templates.TemplateResponse(
        "items.html",
        {
            "request": request,
            "multimedia_dir": multimedia_dir,
            "dirs": dirs_final,
            "images": images,
            "videos": videos,
        },
    )


if __name__ == "__main__":
    uvicorn.run(f"kgimages.app:app", host="0.0.0.0", port=8808, workers=6)
